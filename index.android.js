/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  Form,
  TextInput,
  View
} from 'react-native';

export default class HelloWorld extends Component {

  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Hello android !!
        </Text>

        <TextInput
          placeholder = 'start typing ...'
          editable = {true}
          width = {100}
          onChangeText={(text) => this.setState({text})}
        />

        <Text style={styles.instructions}>
          { this.state.text }
        </Text>

        <Image
          style={{width: 50, height: 50}}
          source={ require('./assets/img/logo.jpg') }
        />
        <Text style={styles.instructions}>
          array101
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('HelloWorld', () => HelloWorld);
